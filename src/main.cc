# include <cstdlib>
# include <iostream>
# include <vector>

# include "dfs.hh"
# include "image.hh"
# include "graph_cut.hh"
# include "utils.hh"

# include "utils/marks.hh"
# include "utils/capacities.hh"
# include "utils/png.hh"

/*
 * source = background
 * target = foreground
 */
int main(int argc, char** argv)
{
    if (argc < 3)
    {
        std::cerr << "Usage: " << argv[0] << " IMAGE MARKS\n";
        return 1;
    }

    // load image
    const auto img = read_image(argv[1]);

    const size_t height = img.height;
    const size_t width = img.width;

    const size_t nb_pixels = height * width;

    marks_t marked_source;
    marks_t marked_target;
    // Output parameters
    init_marks(read_image(argv[2]), marked_source, marked_target);

    auto capacities = capacities_t(nb_pixels);

    int avg_color_source[3];
    average_mark_color(img, marked_source, avg_color_source);
    int avg_color_target[3];
    average_mark_color(img, marked_target, avg_color_target);

    for (size_t i = 0; i < nb_pixels; ++i)
    {
        // distance à la couleur moyenne du marqueur S;
        capacities[i][source_idx] = distance_to_mark(img, avg_color_source, i);

        // distance à la couleur moyenne du marqueur Y;
        capacities[i][target_idx] = distance_to_mark(img, avg_color_target, i);
    }
    // set capacities to max and min if already know the class
    for (size_t i = 0; i < marked_target.size(); ++i)
    {
        capacities[marked_target[i]][target_idx] = 255;
        capacities[marked_target[i]][source_idx] = 0;
    }
    for (size_t i = 0; i < marked_source.size(); ++i)
    {
        capacities[marked_source[i]][source_idx] = 255;
        capacities[marked_source[i]][target_idx] = 0;
    }

    // compute gradient of pixels to init capacities
    compute_gradients(img, height, width, capacities);
    std::cout << "END compute gradient" << std::endl;


    // graphcut algo
    // FIXME: useless indices
    graph_cut(capacities, nb_pixels, width, height, source_idx, target_idx);
    std::cout << "END graph cut" << std::endl;


    // visit pixels from object marks
    auto visited = std::vector<bool>(nb_pixels);
    dfs(capacities, width, height, visited, marked_target);

    std::cout << "END dfs" << std::endl;

    // write output image
    auto gray_image = Image(height, width);
    for (size_t i = 0; i < nb_pixels; ++i)
        if (visited[i])
            gray_image.pixels[i] = {255, 255, 255};
    write_image("output.png", gray_image);
}
