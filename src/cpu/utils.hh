# pragma once

# include <vector>
# include <cstddef>

# include "png.hh"

// fill an array with index of neighbours of current pixel
std::vector<size_t>
get_neighbours(const size_t height_img, const size_t width_img, size_t current);

size_t
neighbour_to_index(size_t curr, size_t neigh, size_t width, size_t height);

void abs(int x[3], const bgr_t& y);
