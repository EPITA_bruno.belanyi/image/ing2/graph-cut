# include "utils.hh"

std::vector<size_t>
get_neighbours(size_t height_img, size_t width_img, size_t current)
{
    size_t x = current % width_img;
    size_t y = current / width_img;

    std::vector<size_t> ans;

    if (y > 0)
        ans.emplace_back((y - 1) * width_img + x);
    if (y < height_img - 1)
        ans.emplace_back((y + 1) * width_img + x);
    if (x > 0)
        ans.emplace_back(y * width_img + x - 1);
    if (x < width_img - 1)
        ans.emplace_back(y * width_img + x + 1);

    return ans;
}

size_t neighbour_to_index(size_t curr, size_t neigh, size_t width,
                          size_t height)
{
    size_t x = curr % width;
    size_t y = curr / width;

    size_t nb = 0;

    if (y > 0)
    {
        if (neigh == curr - width)
            return nb;
        ++nb;
    }
    if (y < height - 1)
    {
        if (neigh == curr + width)
            return nb;
        ++nb;
    }
    if (x > 0)
    {
        if (neigh == curr - 1)
            return nb;
        ++nb;
    }
    if (x < width - 1 && neigh == curr + 1)
        return nb;
    return -1;
}

void abs(int x[3], const bgr_t& y)
{
    for (int i = 0; i < 3; ++i)
    {
        x[i] -= y[i];
        if (x[i] < 0)
            x[i] = -x[i];
    }
}
