# include <algorithm>
# include <cstdlib>
# include <iostream>
# include "utils.hh"
# include "capacities.hh"

static constexpr short HEIGHT_MAX = 50;

static bool is_active(size_t u, const std::vector<short>& excess, const std::vector<short>& height)
{
    return excess[u] > 0 && height[u] < HEIGHT_MAX; // FIXME: why not unsigned
}

static bool any_active(const std::vector<short>& excess, const std::vector<short>& height)
{
    for (size_t i = 0; i < excess.size(); ++i)
        if (is_active(i, excess, height))
            return true;
    return false;
}

static void push(capacities_t& capacities, std::vector<short>& excess, size_t u, size_t v,
                 size_t neigh_u, size_t neigh_v)
{
    short flow_sent = std::min(excess[u], capacities[u][neigh_u]);
    capacities[u][neigh_u] -= flow_sent;
    capacities[v][neigh_v] += flow_sent;
    excess[u] -= flow_sent;
    excess[v] += flow_sent;
}

static void relabel(capacities_t& capacities, std::vector<short>& height, size_t u,
                    const std::vector<size_t>& neighbours_index)
{
    short min_height = HEIGHT_MAX;
    for (size_t i = 0; i < neighbours_index.size(); ++i)
    {
        size_t v = neighbours_index[i];
        if (capacities[u][i] > 0)
            if (short new_height = height[v] + 1; new_height < min_height)
                min_height = new_height; // FIXME: why not unsigned
    }
    height[u] = min_height;
}

static void push_relabel(capacities_t& capacities, std::vector<short>& height, std::vector<short>& excess,
                         const size_t nb_pixels, const std::vector<std::vector<size_t>>& neighbours)
{
    while (any_active(excess, height))
    {
        for (size_t u = 0; u < nb_pixels; ++u)
        {
            if (is_active(u, excess, height))
            {
                bool to_relabel = true;
                for (size_t neigh = 0; neigh < neighbours[u].size(); ++neigh)
                {
                    size_t v = neighbours[u][neigh];
                    if (capacities[u][neigh] > 0 and height[v] < height[u])
                        to_relabel = false;
                }

                if (to_relabel)
                    relabel(capacities, height, u, neighbours[u]);
            }
        }


        for (size_t u = 0; u < nb_pixels; ++u)
        {
            if (is_active(u, excess, height))
            {
                for (size_t neigh = 0; neigh < neighbours[u].size(); ++neigh)
                {
                    size_t v = neighbours[u][neigh];
                    if (capacities[u][neigh] > 0 and height[v] == height[u] - 1)
                    {
                        size_t neigh_v = 0;
                        while (neigh_v < neighbours[v].size() && neighbours[v][neigh_v] != u)
                            ++neigh_v;
                        push(capacities, excess, u, v, neigh, neigh_v);
                    }
                }
            }
        }
    }
}

static void direct_push(const capacities_t& capacities, std::vector<short>& excess, const size_t source,
                        const size_t target, const size_t nb_pixels)
{
    for (size_t u = 0; u < nb_pixels; ++u)
        excess[u] = capacities[u][source] - capacities[u][target];
}

void graph_cut(capacities_t& capacities, const size_t nb_pixels,
               const size_t width_img, const size_t height_img,
               const size_t source, const size_t target)
{
    auto excess = std::vector<short>(nb_pixels);
    auto height = std::vector<short>(nb_pixels);

    direct_push(capacities, excess, source, target, nb_pixels);

    // init neighbours
    std::vector<std::vector<size_t>> neighbours;
    neighbours.reserve(nb_pixels);

    for (size_t i = 0; i < nb_pixels; ++i)
        neighbours.emplace_back(get_neighbours(height_img, width_img, i));

    push_relabel(capacities, height, excess, nb_pixels, neighbours);
}
