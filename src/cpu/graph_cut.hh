# pragma once

# include "capacities.hh"

void graph_cut(capacities_t& capacities, const size_t nb_pixels,
               const size_t width_img, const size_t height_img,
               const size_t source, const size_t target);
