# include <cstdlib>

# include "image.hh"
# include "utils.hh"

static constexpr short SEUIL_COLOR = 150;

short inverse_gradient(int gradient[3])
{
    return 255 / ((gradient[0] + gradient[1] + gradient[2]) / 3 + 1) + 1;
}

short distance_to_mark(const Image& img, int avg_color[3], size_t i)
{
    int col[3];
    for (int k = 0; k < 3; ++k)
        col[k] = avg_color[k];
    abs(col, img.at(i / img.width, i % img.width));
    return inverse_gradient(col);
}

void compute_gradients(const Image& img, const size_t height,
                       const size_t width, capacities_t& capacities)
{
    for (size_t i = 0; i < width * height; ++i)
    {
        auto neighbours = get_neighbours(height, width, i);
        for (size_t n = 0; n < neighbours.size(); ++n)
        {
            // inverse du gradient (niveau de gris)
            int color[3];
            for (int k = 0; k < 3; ++k)
                color[k] = img.at(i / img.width, i % img.width)[k];

            abs(color, img.at(neighbours[n] / img.width, neighbours[n] % img.width));
            capacities[i][n] = inverse_gradient(color);
        }
    }
}

void average_mark_color(const Image& img,
                        const std::vector<size_t>& marked,
                        int avg[3])
{
    for (int i = 0; i < 3; ++i)
        avg[i] = 0;

    for (size_t i = 0; i < marked.size(); ++i)
        for (int k = 0; k < 3; ++k)
            avg[k] += img.at(marked[i] / img.width, marked[i] % img.width)[k];

    for (int i = 0; i < 3; ++i)
        avg[i] /= marked.size();
}
