# pragma once

# include <vector>

# include "capacities.hh"

void dfs(const capacities_t& residual_cp, const size_t width, const size_t height,
         std::vector<bool>& visited, const std::vector<size_t>& linked_to_target);
