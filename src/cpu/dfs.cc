# include <stack>
# include <cstdlib>
# include "dfs.hh"
# include "utils.hh"

void dfs(const capacities_t& residual_cp, const size_t width, const size_t height,
         std::vector<bool>& visited, const std::vector<size_t>& linked_to_target)
{
    std::stack<size_t> s;
    // start with pixels marked as target
    for (size_t i = 0; i < linked_to_target.size(); ++i)
        s.push(linked_to_target[i]);

    while (!s.empty())
    {
        size_t v = s.top();
        s.pop();

        if (!visited[v])
        {
            visited[v] = true;
            std::vector<size_t> neighbours = get_neighbours(height, width, v);
            for (size_t i = 0; i < neighbours.size(); ++i)
            {
                size_t neigh_v = neighbour_to_index(neighbours[i], v, width, height);
                if (residual_cp[neighbours[i]][neigh_v] > 0)
                    s.push(neighbours[i]);
            }
        }
    }
}
