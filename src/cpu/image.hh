# pragma once

# include <cstddef>
# include <vector>

# include "marks.hh"
# include "capacities.hh"
# include "png.hh"

short inverse_gradient(int gradient[3]);

short distance_to_mark(const Image& img, int avg_color[3], size_t i);

void compute_gradients(const Image& img, const size_t height,
                       const size_t width, capacities_t& capacities);

void average_mark_color(const Image& img,
                        const marks_t& marked,
                        int avg[3]);
