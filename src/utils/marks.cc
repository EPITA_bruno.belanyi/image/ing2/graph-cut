#include "marks.hh"

void init_marks(const Image& img, marks_t& source, marks_t& target)
{
    for (size_t j = 0; j < img.height; ++j)
    {
        for (size_t i = 0; i < img.width; ++i)
        {
            const auto& px = img.at(j, i);
            if (px[0] > 0) // blue
                source.push_back(j * img.width + i);
            else if (px[2] > 0) // red
                target.push_back(j * img.width + i);
        }
    }
}
