# include "png.hh"

# include <memory>
# include <stdexcept>

# include <cstdlib>
# include <cstdio>

# include <boost/gil/extension/io/png.hpp>

struct FileCloser
{
    void operator()(FILE* f)
    {
        if (f)
            fclose(f);
    }
};

Image read_image(const std::string& path)
{
    using namespace boost::gil;

    rgb8_image_t img;
    read_and_convert_image(path, img, png_tag());

    const size_t height = img.height();
    const size_t width = img.width();
    auto pixels = pixels_t();
    pixels.reserve(height * width);

    for (size_t y = 0; y < height; ++y)
    {
        const auto it = img._view.row_begin(y);
        for (size_t x = 0; x < width; ++x)
        {
            bgr_t pix{at_c<2>(it[x]), at_c<1>(it[x]), at_c<0>(it[x])};
            pixels.emplace_back(pix);
        }
    }

    return Image{height, width, pixels};
}

void write_image(const std::string& path, const Image& img)
{
    using namespace boost::gil;

    rgb8_image_t out(img.width, img.height);

    for (size_t y = 0; y < img.height; ++y)
    {
        auto it = out._view.row_begin(y);
        for (size_t x = 0; x < img.width; ++x)
        {
            at_c<2>(it[x]) = img.at(y, x)[0];
            at_c<1>(it[x]) = img.at(y, x)[1];
            at_c<0>(it[x]) = img.at(y, x)[2];
        }
    }

    write_view(path, view(out), png_tag());
}
