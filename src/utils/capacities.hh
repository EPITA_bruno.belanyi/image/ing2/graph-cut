# pragma once

# include <array>
# include <vector>

using capacity_t = std::array<short, 6>; // 4 neighbors + source + target

constexpr size_t source_idx = 4;
constexpr size_t target_idx = 5;

using capacities_t = std::vector<capacity_t>;
