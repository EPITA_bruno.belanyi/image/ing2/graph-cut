# pragma once

# include <array>
# include <string>
# include <vector>

using bgr_t = std::array<unsigned char, 3>;
using pixels_t = std::vector<bgr_t>;

struct Image
{
    size_t height;
    size_t width;
    pixels_t pixels; // packed BGR

    Image(size_t h, size_t w, pixels_t p)
    : height(h), width(w), pixels(std::move(p))
    {}

    Image(size_t h, size_t w)
    : height(h), width(w), pixels(h * w)
    {}

    bgr_t& at(size_t h, size_t w)
    {
        return pixels[h * width + w];
    }

    const bgr_t& at(size_t h, size_t w) const
    {
        return pixels[h * width + w];
    }
};


Image read_image(const std::string& path);

void write_image(const std::string& path, const Image& img);
